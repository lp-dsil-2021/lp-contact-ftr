public interface IContactDAO {

    boolean isContactExists(String nom);

    void add(Contact c);
}
