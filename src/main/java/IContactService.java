public interface IContactService {
    
    /**
     * Méthode qui permet de créer un contact
     * @param nom nom du contact à créer
     * @throws ContactException exception lorsque le contact ne peux-être créé (Nom invalide ou Nom unique)
     */
    void creerContact(String nom) throws ContactException;
}
