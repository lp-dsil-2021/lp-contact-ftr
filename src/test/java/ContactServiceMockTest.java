import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {
    @Mock
    private IContactDAO contactDAO;
    @InjectMocks
    private ContactService contactService = new ContactService();

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Test (expected = ContactException.class)
    public void shouldFailOnDuplicateEntry() throws ContactException {
        //Def
        Mockito.when(contactDAO.isContactExists("thierry")).thenReturn(true);
        //Test
        contactService.creerContact("thierry");
    }

    @Test (expected = ContactException.class)
    public void shouldFaillIfEmpty() throws ContactException {
        //Test
        contactService.creerContact("");
    }

    @Test
    public void shouldPass() throws ContactException {
        //Def
        Mockito.when(contactDAO.isContactExists("Thierry")).thenReturn(false);
        //Test
        contactService.creerContact("Thierry");
        Mockito.verify(contactDAO).add(contactCaptor.capture());
        //Je souhaite voir le contenu de l'argument
        Contact c = contactCaptor.getValue();
        Assertions.assertEquals("Thierry", c.getNom());
    }

    @Test (expected = ContactException.class)
    public void shouldFaillIfNull() throws ContactException {
        //Test
        contactService.creerContact(null);
    }

    /*@Test (expected = ContactException.class)
    public void shouldDelete() throws ContactException {
        //Def
        Mockito.when(contactDAO.isContactExists("Thierry")).thenReturn(true);
        //Test
        contactService.deleteContact("thierry");
    }*/


}
